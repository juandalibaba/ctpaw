from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('pregunta/<int:id_pregunta>', views.pregunta, name='pregunta'),
    path('votar/<int:id_pregunta>', views.votar, name='votar'),
    path('resultados', views.resultados, name='resultados'),
    path('hola', views.hola, name='hola')
]