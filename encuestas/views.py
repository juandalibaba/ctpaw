from django.shortcuts import render, reverse
from django.http import  HttpResponse, Http404, HttpResponseRedirect
from .models import Pregunta, Opcion, Sesiones


def index(request):

    preguntas = Pregunta.objects.filter(primera_pregunta=True)
    if len(preguntas) == 0:
        raise Http404("Hay que definir una primera pregunta")

    primera_pregunta = preguntas[0]
    return render(request, 'encuestas/index.html', {'id_pregunta': primera_pregunta.id})


def pregunta(request, id_pregunta):
    try:
        _pregunta = Pregunta.objects.get(pk=id_pregunta)
    except Pregunta.DoesNotExist:
        raise Http404("Esa pregunta no existe")

    if _pregunta.primera_pregunta:
        sesiones = Sesiones.objects.all()
        if len(sesiones) == 0:
            sesion = Sesiones()
        else:
            sesion = sesiones[0]

        sesion.numero += 1
        sesion.save()

    template = 'encuestas/pregunta_multiple.html' if _pregunta.opcion_multiple  else 'encuestas/pregunta.html'

    return render(request, template, {'pregunta': _pregunta})


def votar(request, id_pregunta):
    print(request.POST)
    try:
        _pregunta = Pregunta.objects.get(pk=id_pregunta)
    except Pregunta.DoesNotExist:
        raise Http404("Esa pregunta no existe")

    try:
        for opcion in request.POST.getlist('opcion'):
            opcion_seleccionada = _pregunta.opcion_set.get(pk=opcion)
            opcion_seleccionada.votos += 1
            opcion_seleccionada.save()
            # Always return an HttpResponseRedirect after successfully dealing
            # with POST data. This prevents data from being posted twice if a
            # user hits the Back button.

    except (KeyError, Opcion.DoesNotExist):
        return render(request, 'encuetas/pregunta.html', {
            'pregunta': pregunta(),
            'error_message': "No has seleccionado ninguna opción.",
        })
    else:
        if _pregunta.siguiente_pregunta:
            return HttpResponseRedirect(reverse('pregunta', args=(_pregunta.siguiente_pregunta.id,)))
        else:
            return HttpResponseRedirect(reverse('resultados'))

def resultados(request):
    preguntas = Pregunta.objects.all()
    sesiones = Sesiones.objects.all()[0]

    return render(request, 'encuestas/resultados.html', {'preguntas': preguntas, 'sesiones': sesiones})

def hola(request):
    nombre = request.POST["nombre"] if request.POST.get("nombre") else ""
    asignatura = request.POST["asignatura"] if request.POST.get("asignatura") else ""
    return render(request, 'encuestas/hola.html', {
        "method": request.method,
        "nombre": nombre,
        "asignatura": asignatura
    })
