from django.db import models

class Pregunta(models.Model):
    texto = models.TextField(max_length=512)
    fecha = models.DateTimeField()
    opcion_multiple = models.BooleanField(default=False)
    primera_pregunta = models.BooleanField(default=False)
    siguiente_pregunta = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.texto


class Opcion(models.Model):
    texto = models.TextField(max_length=512)
    votos = models.IntegerField(default=0)
    pregunta = models.ForeignKey(Pregunta, on_delete=models.CASCADE)

    def __str__(self):
        return self.texto


class Sesiones(models.Model):
    numero = models.IntegerField(default=0)