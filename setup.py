
from setuptools import setup, find_packages

setup(
   name='misitio',
   packages=find_packages(),
   version='0.1.13',
   author='Juan David Rodríguez García',
   description='Programa de ejemplo para el curso CTPAW',
   author_email='juanda@juandarodriguez.es',
   include_package_data=True,
   install_requires=[
        'asgiref==3.4.1',
        'Django==4.0.1',
        #'django-environ==0.8.1',
        'django-mathfilters==1.0.0',
   ],
   setup_requires=['pytest-runner'],
   tests_require=['pytest'],
   scripts=['manage.py']
)
